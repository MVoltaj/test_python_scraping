import requests
import gzip
import json
import csv
import selenium

def http_request():
    return requests.post(url="https://httpbin.org/anything",data={"isadmin": 1}).text

def exercise_two():

    #i'm using a function here, as i think it's way more appropriate for this exercise

    #I have changed the first object of the json because it was saying "product" instead of "products"
    f = open('Products.csv','w',newline='')
    writer = csv.writer(f)
    writer.writerow(["Name","Price"])

    #Opening the file and converting it to json
    with gzip.open('data/data.json.gz', 'rb') as f:
        data = json.loads(f.read())

    for products in data["Bundles"]:
        index = 0
        if "Products" in products and "Name" in products:
            product_name = products["Name"][0:30]
            if "Promotion" in products["Products"][0]:
                #If there is a promotion on the product, we cant access to products["Products"][0]
                # where we should get our variable as it's allocated to promotion, so we put index = 1 to get them
                index = 1
            if products["Products"][index]["Price"] != None:
                price = round(products["Products"][index]["Price"],1)
            else:
                price = "null"
            isinStock = products["Products"][index]["IsInStock"]
            if isinStock == False:
                product_id = products["Products"][index]["Barcode"] #Fetching Product ID
                print("Product ID : " + product_id + " - Product Name : " + product_name)
            if price !="null":
                print("You can buy " + product_name + " at our store at " + str(price) + "€")
                writer.writerow([product_name,str(price)])
            else:
                print("No price Found for " + product_name)
        else:
            print("Error - The products isn't well scrapped")


def exercise_three():
	#On this exercise, i tried using a scrapy spider, but the fact is that products on the page are redendered by JS, which makes it way harder to access through spider. I 	#tried using scrapy-slash but without any success. 



exercise_three()
import random
import functools
import inspect

def random_gen():
    g = 11
    while g != 15:
        g = random.randint(10,20)
        yield g


def decorator_to_str(func):
    def wrapper(*a,**kw):
        string = str(func(*a,**kw))
        return string
    return wrapper


@decorator_to_str
def add(a, b):
    return a + b

@decorator_to_str
def get_info(d):
    return d['info']


def ignore_exception(*exceptions):
    def decorator(test_func):
        @functools.wraps(test_func)
        def wrapper(*args, **kwargs):
            try:
                return test_func(*args, **kwargs)
            except exceptions as e:
                return None
        return wrapper
    return decorator



@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]
        return _wrap


class MetaInherList(list):
    pass


class Ex:
    x = 4

"""
class ForceToList(Ex, metaclass=MetaInherList):
    def __call__(self, func):
        return Ex.x
"""

class ForceToList(Ex):
    __metaclass__ = MetaInherList
    def __call__(self, func):
        return Ex.x



class metaClass: #Exercise 6
    def __init__(self, metaclass):
        self.metaclass = metaclass

    def __call__(self):
        if 'process' in dir(self.metaclass) and inspect.isfunction(self.metaclass.process): # Checks if process is attribute + if it's a method
            if len(inspect.getfullargspec(self.metaclass.process).args) == 3: # Checks if it requires 3 arguments
                return True
        return False



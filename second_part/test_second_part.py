import pytest

from src import div, raise_something, add, ForceToList, random_gen, get_info, CacheDecorator, metaClass


def test_generator():
    g = random_gen()
    assert isinstance(g, type((x for x in [])))
    a = next(g)
    while a != 15:
        assert 10 <= a <= 20
        a = next(g)
    with pytest.raises(StopIteration):
        next(g)
test_generator()

def test_to_str():
    assert add(5, 30) == '35'
    assert get_info({'info': [1, 2, 3]}) == '[1, 2, 3]'

test_to_str()
def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)

test_ignore_exception()

def test_meta_list():
    test = ForceToList([1, 2])
    assert test[1] == 2
    assert test.x == 4



def add(a, b):
    return a + b

def hello():
    return 5

def multiply(a, b):
    return a * b

def test_Cache_decorator():
    c = CacheDecorator()
    d = c(add)(1, 3)
    print(c.cache)
    assert c.cache[1] == 4 #Working
    c = CacheDecorator()
    f = c(multiply)(4, 5)
    print(c.cache)
    assert c.cache[4] == 20 #Not working
    c = CacheDecorator()
    e = c(hello)()
    print(c.cache)  #not working, because no argument were passed to the function hello

#test_Cache_decorator()

#Test for Exercise 6
class testing:
    def process(first, second, third):
        return True

class testing2:
    def processing(first,second,third):
        return True
def test_exercise_6():
    i = metaClass(testing)
    assert i() == True
    j = metaClass(testing2)
    assert j() == False

test_exercise_6()
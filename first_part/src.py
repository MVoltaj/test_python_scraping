def exercise_one():
    for i in range(0,100):
        if i % 3 == 0 and i % 5 == 0:
            print("Threefive")
        elif i % 3 ==0:
            print("Three")
        elif i % 5 == 0:
            print("Five")
        else:
            print(str(i))

def exercise_two(n):
    temp = []
    dico = {}
    s = str(n)
    for i in range(len(s)):
        for j in range(i, len(s)):
            temp.append(s[i:j + 1])

    for l in range(len(temp)):
        mx = 1
        p = temp[l]
        for k in range(len(p)):
            mx *= int(p[k])
        dico[p] = mx

    temp0 = []

    for k, v in dico.items():
        if v not in temp0:
            temp0.append(v)
    if len(temp0) == len(temp):
        return True
    else:
        return False


def exercise_three(a):
    if type(a) == list:
        x = 0
        for letter in a:
            if type(letter) == str:
                if letter.isdigit() or letter.lstrip('-').isdigit():
                    if int(letter) > 10:
                        return False
                    else:
                        x += int(letter)
        return x
    else:
        return False


def exercise_four(given_word,list_words):
    x = sorted(list(given_word))
    c = list()
    for word in list_words:
        if sorted(list(word)) == x:
            c.append(word)
    return c


'''
print(exercise_four('abba', ['aabb', 'abcd', 'bbaa', 'dada']))


print(exercise_four('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']))
print(exercise_four('laser', ['lazing', 'lazy',  'lacer']))



print(exercise_three(['4', '3', '-2']))
print(exercise_three(432))
print(exercise_three(['nothing', 3, '8', 2, '1']))
print(exercise_three(['54']))

'''



